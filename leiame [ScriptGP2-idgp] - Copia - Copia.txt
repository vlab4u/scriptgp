Programa: ScriptGP2-idlates-idgp.py
Versão: 2015.02.18

DESCRIÇÃO 

O programa retorna uma lista (idgp) com todos os grupos de pesquisa aos quais um pesquiador (idlattes) pertence.

É possível passar para o programa uma lista de pesquisadores, por meio dos seus respectivos numeros de identificação na plataforma Lattes (idllates).

Para descobrir o número de identificação de um pesquisador na plataforma lattes basta verificar o seu endereço (URL) de acesso ao currículo.
Por exemplo, http://lattes.cnpq.br/1908141029874463, o idlattes é o conjunto de 16 dígitos, 1908141029874463.
 
APLICAÇÃO

Este programa foi desenvolvido para auxiliar na identificacao da rede de pesquisadores por meio da participação em grupos de pesquisa.

INSTALAÇÃO

O ScriptGP2-idlates-idgp.py precisa dos seguintes programas
1. Python 2.7 (https://www.python.org/)
2. Navegador Firefox (https://www.mozilla.org/)
3. Biblioteca Selenium para Python (https://pypi.python.org/pypi/selenium)


CONFIGURAÇÃO

O programa precisa de um arquivo como entrada de dados:
1 - arquivo com a lista de idlattes (pesquisadores).
    O arquivo deverá conter apenas um idlattes por linha.
    O idlattes deverá ser os primeiros 16 caracteres de cada linha.
    Exemplo
    1908141029824463, João Fulano
    1992884798282718, Paulo Beltrano


Configurar os arquivos de entrada e saída

1) Abra o arquivo ScriptGP2-idlates-idgp.py

2) No início do programa existe um cabeçalho indicando #SETTINGS

3) Logo abaixo do cabeçalho você encontrará as variáveis para configuração.

4) Lista de idlattes - lista com todos os idlattes a serem pesquisados. Os primeiros 16 caracteres de cada linha devem corresponder a um idlates
    Configure a variável infile, escrevendo o caminho para o arquivo com a lista de idlattes. O caminho deverá ficar entre as aspas.
    exemplo: infile= r"C:\Users\amsantos\Google Drive\Programas\ScriptGP20\Teste Nano\nanogpbp.list"

5) Local para gravar os resultados 
    Configure a variável oufile, escrevendo o caminho para o arquivo de saída. O caminho deverá ficar entre as aspas.
    exemplo: oufile = r'C:\Users\amsantos\Google Drive\Programas\ScriptGP20\Teste Nano\teste-02-02-2015'

USO
python ScriptGP2-idlates-idgp.py

OBSERVAÇÕES IMPORTANTES NO USO
Devido ao grande número de consultas, é recomendável uma conexão de internet de boa qualidade. 
O programa possui recursos para recuperação de erros e tenta retomar a pesquisa caso ocorram problemas momentâneos com o servidor ou internet.
O programa pode levar vários minutos,  horas ou até dias para processar, dependendo do tamanho da lista de pesquisadaores.
O programa irá abrir e fechar janelas do Firefox automaticamente. Não interfira nesta operação pois resultará em erro no processamento e parada do programa.
Recomenda-se que o computador não seja utilizado para outros fins (ou pelo menos para internet) enquanto o programa estiver sendo executado.
Na tela de console será exibido o andamento do processamento.
Certifique-se de que seu computador ou notebook não esteja configurado para entrar em modo de hibernação. 

RESULTADOS
O programa gera dois  arquivos com resultados, além de exibir no terminal o andamento do processo.

Os arquivos de resultados são os seguintes:

1 - [prefixo]idgpresults.list
    This a log for all idllates searched, with three columns. The default separator is tab.
    The first column is a idlattes searched.
    The second column is a idgp number.
    The third column is a idgp name.
    
    Example:
    6858139747014559 <tab> 7371407724901257 <tab> Nanotecnologia Aplicada ao Agronegócio
    
2 - [prefixo]listaidlattes_erros.list (this is a list with all idllates that coufd not be processed due a undefined problem). 
    You can try to reprocess just the listaidlatteserros, puting this list as a input for the program. 
    DANGER: Remember to modify your output file (oufile) configuration in this case. Otherwise, your first result files will be overwritten.
    
3 - [prefixo]idgpScripLattes.list
    This is a list of all idlattes and respective idgps for input in ScriptLattes.
    The list has one idlattes per line and groups are splited by :: characters. 
    Each line contains idlattes, the group name, group institution and idgp.
    Each group idgp are inside brackets [].
    
    For example
    
    3534665149331526, , , GTEL - Grupo de Pesquisa em Telecomunicações Sem-fio - UFC[9385111923569787] :: Processamento de Sinais e Informação - UFC[8102927564533498]
    
    A researcher with idlattes 3534665149331526, 
   
    are engajed in group GTEL - Grupo de Pesquisa em Telecomunicações Sem-fio, 
    that pertains to UFC institution, and has a idgp number 9385111923569787
    
    AND
    
    are engajed in group Processamento de Sinais e Informação,
    that pertains to UFC institution, and has a idgp number 8102927564533498.
    
    
    
    
    