﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Programa: ScriptGP2-pvt-pesquisadores.py

#
# Função: O programa recebe uma lista de cadastro de grupos e outra com pesquisadores monta uma pivottable.
# A lista de cadastro deve conter o idgp e demais dados como instituicao, estado, ano, etc
# A lista de pesquisadores deve conter o idlattes e o grupo ao qual pertence (idgp)


#=============================================================
#SETTINGS
#=============================================================

#set the tab character to be used in output
sep = '\t' 

#arquivo de entrada com a lista de grupos de pesquisa no formato 
infile_grupos = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano-dedup-idgps-unicos-idgpcadastro-cleaned.list"

#arquivo com a lista de pesquisadores e os grupos aos quais pertencem 
infile_pesquisadores = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano-dedup-26-03-2015-idgp-idlattes.list"

#arquivo com a lista de estudantes e os grupos aos quais pertencem
infile_estudantes = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano-dedup-estudantes-idgp-idlattes-est.list"

#titulopagina e tabelas
tituloPaginaPesquisador = r"Grupos de Pesquisa em Nanotecnologia - Pesquisadores (2015)"
tituloTabelaPesquisador = r"Grupos de Pesquisa em Nanotecnologia - Pesquisadores (2015)"
tituloPaginaEstudante = r"Grupos de Pesquisa em Nanotecnologia - Estudantes (2015)"
tituloTabelaEstudante = r"Grupos de Pesquisa em Nanotecnologia - Estudantes (2015)"

#diretorio para os arquivos de saída. Deve terminar com '\'

oudir = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\pivot\\"
ouprefix = r"nano-"
htmldir = './'

oufile = oudir+ouprefix





import json
import re
import unicodedata
def pivottable(titulopagina, titulotabela, nomearquivojson):
    html = r'''
<!DOCTYPE html>
<html>
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="content-type">
    <title>SCRIPTGP_TITULO_PAGINA</title>
    <link rel="stylesheet" type="text/css" href="dist/pivot.css">
    <script type="text/javascript" src="dist/d3.v3.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="dist/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="dist/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="dist/jquery.csv-0.71.min.js"></script>
    <script type="text/javascript" src="dist/pivot.js"></script>
    <script type="text/javascript" src="dist/gchart_renderers.js"></script>
    <script type="text/javascript" src="dist/d3_renderers.js"></script>
    <script type="text/javascript" src="dist/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="dist/export_renderers.js"></script>
    <script type="text/javascript" src="dist/pivot.fr.js"></script>
    <script type="text/javascript" src="dist/pivot.pt.js"></script>
    <script type="text/javascript" src="dist/pivot.es.js"></script>
    <script type="text/javascript" src="dist/pivot.en.js"></script>   
  </head>
  <style>
        * {font-family: Verdana;}
        .node {
          border: solid 1px white;
          font: 10px sans-serif;
          line-height: 12px;
          overflow: hidden;
          position: absolute;
          text-indent: 2px;
        }
    </style>
  <body>
    <h1>SCRIPTGP_TITULO_TABELA</h1>
    <script type="text/javascript">
            google.load("visualization", "1", {packages:["corechart", "charteditor"]});
            $(function(){
                var lingua = "en", rendererName = "Line Chart";
                var userLang = (navigator.language || navigator.userLanguage).split("-")[0];
                    if (userLang == "pt") {
                        var lingua="pt",
                            rendererName = "Grafico de Linhas";
                    } else if (userLang == "fr") {
                        var lingua="fr",
                            rendererName = "Graphique de Courbes";
                    } else {
                        var lingua="en";
                        rendererName = "Line Chart"
                    }
                var derivers = $.pivotUtilities.derivers;
                var renderers = $.pivotUtilities.locales[lingua].renderers;
                $.getJSON("SCRIPTGPJSONDATA", function(mps) {
                    $("#output").pivotUI(mps, {
                    renderers: renderers,
                    cols: ["Ano"],
                    rendererName: rendererName
                    },false,lingua);
                });
             });
    </script>
        <div id="output" style="margin: 30px;"></div>
    <br>
    <a href="https://github.com/nicolaskruchten/pivottable/wiki/UI-Tutorial" target="_blank">Support for the use of the interface!!!</a>
    <br>
    Built with <a href="https://github.com/nicolaskruchten/pivottable" target="_blank">PivotTable.js</a>
  </body>
</html>
'''
    html = re.sub("SCRIPTGP_TITULO_PAGINA",titulopagina,html)
    html = re.sub("SCRIPTGPJSONDATA",nomearquivojson,html)
    html = re.sub("SCRIPTGP_TITULO_TABELA",titulotabela,html)
    return html

#=============================================================
#MAIN PROGRAM
#=============================================================
grupos=dict()
pivottableJson=[]
listaIdlattesErros=[]
with open(infile_grupos, 'r') as fi:
    for k in fi:
        c = k.split(sep)
      
        grupos[c[0]]=c
with open(infile_pesquisadores, 'r') as fi:
    for k in fi:
        #o arquivo e separado por tabulacoes e split divide cada coluna em um iten de lista
        #a ordem das colunas e: idgp,idlattes,nomePesquisador
        p = k.split(sep)
        idgp = p[0]
        idlattes = p[1]
        nomePesquisador = p[2]
        if idgp in grupos:
            c = grupos[idgp]
            nomeGrupo = c[1]
            anoFormacaoGrupo = c[3]
            areaGrupo = c[7]
            subAreaGrupo = c[8]
            instituicaoGrupo = c[9]
            siglaInstituicaoGrupo = c[10]
            ufGrupo = c[12]
            cidadeGrupo = c[13]
            pivottableJson.append({
                "Idgp":idgp,
                "NomeGrupo":nomeGrupo,
                "Ano":anoFormacaoGrupo,
                "Area":areaGrupo,
                "SubArea":subAreaGrupo,
                "Instituicao":instituicaoGrupo,
                "SiglaInst.":siglaInstituicaoGrupo,
                "UF":ufGrupo,
                "Cidade":cidadeGrupo,
                "Idlattes":idlattes,
                "NomePesquisador":nomePesquisador})
        else:
            listaIdlattesErros.append(idlattes+sep+nomePesquisador+sep+idgp+sep+"Grupo nao encontrado")
    
    with open(oufile+'pivottable-erros-pesquisadores.list', 'w') as fo:
        for linhas in listaIdlattesErros:
            fo.write("%s\n" % linhas.encode('utf-8'))
    with open(oufile+'pivottable_pesquisadores.json', 'w') as fo:
        json.dump(pivottableJson,fo)
    
    pagina = pivottable(tituloPaginaPesquisador,tituloTabelaPesquisador,(htmldir+ouprefix+'pivottable_pesquisadores.json'))
    with open(oufile+r'pivottable_pesquisadores.html', 'w') as fo:
        fo.writelines(pagina)

pivottableJson=[]
if infile_estudantes:
    with open(infile_estudantes, 'r') as fi:
        for k in fi:
            #o arquivo e separado por tabulacoes e split divide cada coluna em um iten de lista
            #a ordem das colunas e: idgp,idlattes,nomeEstudante
            p = k.split(sep)
            idgp = p[0]
            idlattes = p[1]
            nomeEstudante = p[2]
            if idgp in grupos:
                c = grupos[idgp]
                nomeGrupo = c[1]
                anoFormacaoGrupo = c[3]
                areaGrupo = c[7]
                subAreaGrupo = c[8]
                instituicaoGrupo = c[9]
                siglaInstituicaoGrupo = c[10]
                ufGrupo = c[12]
                cidadeGrupo = c[13]
                pivottableJson.append({
                    "Idgp":idgp,
                    "NomeGrupo":nomeGrupo,
                    "Ano":anoFormacaoGrupo,
                    "Area":areaGrupo,
                    "SubArea":subAreaGrupo,
                    "Instituicao":instituicaoGrupo,
                    "SiglaInst.":siglaInstituicaoGrupo,
                    "UF":ufGrupo,
                    "Cidade":cidadeGrupo,
                    "Idlattes":idlattes,
                    "NomeEstudante":nomeEstudante})
            else:
                listaIdlattesErros.append(idlattes+sep+nomeEstudante+sep+idgp+sep+"Grupo nao encontrado")
        
#grava os arquivos de saida

with open(oufile+'pivottable-erros-estudantes.list', 'w') as fo:
    for linhas in listaIdlattesErros:
        fo.write("%s\n" % linhas.encode('utf-8'))
with open(oufile+'pivottable_estudantes.json', 'w') as fo:
    json.dump(pivottableJson,fo)
    
#gera e grava a pagina html

pagina = pivottable(tituloPaginaEstudante,tituloTabelaEstudante,(htmldir+ouprefix+'pivottable_estudantes.json'))
with open(oufile+r'pivottable_estudantes.html', 'w') as fo:
    fo.writelines(pagina)

