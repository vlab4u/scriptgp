Programa: ScriptGP2-busca-idlates.py
Versão: 2015.02.26

DESCRIÇÃO 

O programa realiza uma busca em todos os grupos de pesquisa ao qual um pesquisador pertence e retorna se encontrou algum dos termos procurados.

É possível passar para o programa uma lista de pesquisadores, por meio dos seus respectivos numeros de identificação na plataforma Lattes.

Para descobrir o número de identificação de um pesquisador na plataforma lattes basta verificar o seu endereço (URL) de acesso ao currículo.
Por exemplo, http://lattes.cnpq.br/1908141029874463, o idlattes é o conjunto de 16 dígitos, 1908141029874463.
 
APLICAÇÃO

Este programa foi desenvolvido para auxiliar na identificacao de pesquisadores "core competences" em diversos campos científicos, a partir das bases de dados do CNPQ, Brasil.
Para ser considerado um pesquisador "core competence", o mesmo deve possuir algum dos termos de busca tanto na sua base curricular Lattes, quanto em algum dos grupos de pesquisa em que participa.

INSTALAÇÃO

O ScriptGP2 precisa dos seguintes programas
1. Python 2.7 (https://www.python.org/)
2. Navegador Firefox (https://www.mozilla.org/)
3. Biblioteca Selenium para Python (https://pypi.python.org/pypi/selenium)


CONFIGURAÇÃO

O programa precisa de dois arquivos como entrada de dados:
1 - arquivo com a lista de idlattes (pesquisadores).
2 - arquivo com os termos de busca.

Configurar os arquivos de entrada e saída

1) Abra o arquivo ScriptGP2-busca-idllates.py

2) No início do programa existe um cabeçalho indicando #SETTINGS

3) Logo abaixo do cabeçalho você encontrará as variáveis para configuração.

4) Lista de idlattes - lista com todos os idlattes a serem pesquisados. Os primeiros 16 caracteres de cada linha devem corresponder a um idlates
    Configure a variável infile, escrevendo o caminho para o arquivo com a lista de idlattes. O caminho deverá ficar entre as aspas.
    exemplo: infile= r"C:\ScriptGP20\Teste_Nano\nanogpbp.list"

5) Lista com as palavras de busca - lista com os termos de busca. Use apenas um termo por linha. 
    Para termos compostos, coloque todos na mesma linha. Exemplo: nanoestruturas de carbono
    Configure a variável infilesearch, escrevendo o caminho para o arquivo com a lista de busca
    exemplo: infilesearch = r"C:\ScriptGP20\Teste_Nano\lista-termosdebusca-nano.txt"

6) Local para gravar os resultados 
    Configure a variável oufile, escrevendo o caminho para o arquivo de saída. O caminho deverá ficar entre as aspas.
    exemplo: oufile = r'C:\ScriptGP20\Teste_Nano\teste-02-02-2015'

7) Não use espaços, acentuação e cedilhas em caminhos para arquivos e pastas no sistema de arquivos.
    
USO
python ScriptGP2-busca-idllates.py

OBSERVAÇÕES IMPORTANTES NO USO
Devido ao grande número de consultas, é recomendável uma conexão de internet de boa qualidade. 
O programa possui recursos para recuperação de erros e tenta retomar a pesquisa caso ocorram problemas momentâneos com o servidor ou internet.
O programa pode levar várias horas ou até dias para processar, dependendo do tamanho da lista de pesquisadaores e termos.
O programa irá abrir e fechar janelas do Firefox automaticamente. Não interfira nesta operação pois resultará em erro no processamento e parada do programa.
Recomenda-se que o computador não seja utilizado para outros fins (ou pelo menos para internet) enquanto o programa estiver sendo executado.
Na tela de console será exibido o andamento do processamento.
Certifique-se de que seu computador ou notebook não esteja configurado para entrar em modo de hibernação. 

RESULTADOS
O programa gera quatro arquivos com resultados, além de exibir no terminal o andamento do processo.

Os arquivos de resultados são os seguintes:

1 - idlattesfound.list
    This is a list with all idllates that have at least one search term founded in your associated research groups.

2 - idlattesnotfound.list 
    This is a list with all idllates that NOT have anyone search terms.

3 - allresults.list
    This a log for all idllates searched, with four columns. The default separator is tab.
    The first column is a idlattes searched.
    The second column is a search status result (FOUND or NOT FOUND).
    The third column is a code that identifies the research group searched.
    The fourth column is a name of the research group.
    The fith column is a last term searched. (p.ex. if the the first term of a list is founded, the other terms does not needed to be searched!
    Example:
    6858139747014559 <tab> FOUND <tab> 7371407724901257 <tab> Nanotecnologia Aplicada ao Agronegócio <tab> nanocomposito
    
4 - listaidlattes_erros.list (this is a list with all idllates that coufd not be processed due a undefined problem). 
    You can try to reprocess just the listaidlatteserros, puting this list as a input for the program. 
    DANGER: Remember to modify your output file (oufile) configuration in this case. Otherwise, your first result files will be overwritten.

Important Note
# For each idllates provided, the program interate over the serch terms list and stops when found the first match or reach the end of list.
# Note that after a positive result, the program does not continue searching over other groups of the researcher. They jump for the next researcher in the list.
# Thus, the line in allresults.list contains just the FIRST term in the FIRST group founded for each reseracher (idlattes)
# May be possible that more terms or groups would be found if the search continued.


