#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Programa: ScriptGP2-busca.py
# Versao: 2014.09.22
#
# Fun��o: O programa recebe uma lista de idgps do diretorio de grupos de pesquisa 
# e extrai informacoes descritivas do grupo
# Programador: Andr� Santos 

import time
import sys
import re
import math
import unicodedata
import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

#=============================================================
#SETTINGS
#=============================================================

# #set the tab character to be used in output
# sep = '\t' 

# #caminho e prefixo para o arquivo de sa�da
# oufile = r'C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\dengue-15-03-2015-'

# #termo de busca
# busca = 'inovacao'

# #tipo de busca
# # 1 - todas as palavras
# # 2 - qualquer palavra
# # 3 - busca exata
# tipoBusca = '2'

def buscaGrupos(tipoBusca,buscarepercussoes,termosBusca,oufile):
    erro = False
    try:
        #starts browser driver 
        browser = webdriver.Firefox()

        #open the searh main page
        browser.get('http://dgp.cnpq.br/dgp/faces/consulta/consulta_parametrizada.jsf')

        #locate the form object
        login_form = browser.find_element_by_id('idFormConsultaParametrizada:idTextoFiltro')

        #fill the form with search term
        login_form.send_keys(termosBusca)

        #select search option
        select = Select(browser.find_element_by_xpath(".//*[@id='idFormConsultaParametrizada:basePesquisa']"))
        select.select_by_value(tipoBusca)
        
        #check if busca por repercussoes do grupo
        if buscarepercussoes:
            browser.find_element_by_xpath((".//*[@id='idFormConsultaParametrizada:campos:3']")).click()

        #submitt the form
        browser.find_element_by_id("idFormConsultaParametrizada:idPesquisar").click()
        browser.implicitly_wait(10)

        #takes the main window reference
        paginaprincipal = browser.current_window_handle

        #encontra o texto com o total de resultados na p�gina
        textoTotalResultados = browser.find_element_by_xpath(("//span[@class='ui-paginator-current']")).text

        #extrai o total de resultados como um n�mero
        totalResultados = int(re.search('[\d].*',textoTotalResultados).group())

        #calcula o total de p�ginas em fun��o do total de resultados (considerando 15 resultados por p�gina)
        totpaginas = int(math.ceil(totalResultados / 15.0))
    except:
        erro = True
        return (False)
    listagrupos=[]
    listagruposerros=[]
    for pagina in range(0,totpaginas,1):
        #get the list of groups objects
        time.sleep(5)
        try:
            grupos  = browser.find_elements_by_xpath("html/body/div[3]/div/div/form[2]/span/div[1]/div[1]/ul/li[*]/div/div[1]/div/a")
        except:
            erro = True
            print "erro ao tentar pegar a lista de botoes de grupos"
            return (False)
        nroerros = 0
        for grupo in grupos:
            time.sleep(3)
            clicou = True
            try:
                nomegrupo = grupo.text
            except:
                nomegrupo = 'nao foi poss�vel identificar o nome do grupo'
            try:
                grupo.click() #open the link of relative group in results page by simulating a click on it
            except:
                erro = True
                clicou = False
                print "Erro ao tentar clicar no botao do grupo "+nomegrupo
            if clicou:
                
                tentativas = 0
                sucesso = False
                #testa se a p�gina abriu corretamente e houve resposta do servidor. realiza ate 5 tentativas
                while not sucesso and tentativas < 5:
                    if len(browser.window_handles) <= 1:
                        print 'Esperando resposta do servidor'
                        time.sleep(10)
                        tentativas = tentativas + 1
                    else:
                        sucesso = True
                #se a pagina abriu, prossegue com a extracao dos dados
                if sucesso:
                    
                    try:
                        print "numero de paginas abertas " + str(len(browser.window_handles))
                        browser.switch_to_window(browser.window_handles[1]) # set the focus on the new window (group window)
                        #wait page loading
                        browser.implicitly_wait(10)
                        if browser.current_url == 'http://dgp.cnpq.br/dgp/faces/errorPage.jsf':
                            print 'erro no servidor do CNPQ ao tentar acessar o grupo ' + nomegrupo
                        else:
                            nomegp = browser.find_elements_by_xpath('html/body/div[3]/div/div/div/div/div[2]/form/div/div[1]/h1')[0].text
                            idgp = browser.current_url[-16:]
                            listagrupos.append(idgp + sep + nomegp + sep + browser.current_url) #append the group url to listagrupos
                            print '[' + idgp + '] '+ nomegp
                        browser.close() #close the group windows (to minimize resources usage like RAM, ex)
                        browser.switch_to_window(paginaprincipal) # set the focos to main windows (results page)
                    except:
                        erro = True
                        #verifica se a janela com o erro ficou aberta e a fecha.
                        # if len(browser.window_handles) > 1:
                            # browser.switch_to_window(browser.window_handles[1])
                            # browser.close()
                        browser.switch_to_window(paginaprincipal)
                        print "Erro ao tentar acessar a pagina do grupo "+nomegrupo
                        listagruposerros.append(nomegrupo)
                if erro:
                    print "Erro na pagina de busca atual. Avancando para proxima pagina."
                   #break
            #click for next page
        try:
            browser.switch_to_window(paginaprincipal)
            browser.find_elements_by_class_name("ui-paginator-next")[0].click()
            browser.implicitly_wait(10)
        except:
            erro = True
            print "erro ao tentar clicar na proxima pagina"
            return (False)
    with open(oufile+'idgp.list', 'w') as fo:
        for linhas in listagrupos:
            fo.write("%s\n" % linhas.encode('utf-8'))
    with open(oufile+'idgp-erros.list', 'w') as fo:
        for linhas in listagruposerros:
            fo.write("%s\n" % linhas.encode('utf-8'))
    return True
#=============================================================
#MAIN PROGRAM
#=============================================================
#arquivoConfig = sys.argv[1]
arquivoConfig = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano.config"
opcoes={}
with open(arquivoConfig, 'r') as fi:
    config = []
    for k in fi:
        opcoes[k.split("=")[0].strip()] = k.split("=")[1].strip()
arquivolistabusca = opcoes['arquivolistabusca']
oufile = opcoes['arquivosaida']
#o decoce string escape permite interpretar o \t corretamente como tabulacao
sep = opcoes['sep'].decode('string_escape')
repercussoes_valor = opcoes['repercussoes'].decode('utf-8')
if remove_accents(repercussoes_valor).lower() == 'sim':
    repercussoes = True
else:
    repercussoes = False

with open(arquivolistabusca, 'r') as fi:
    i = 0
    for k in fi:
        i = i + 1
        tentativas = 1
        sucesso = False
        while not sucesso and tentativas <= 5:
            tipoBusca = k.split(",")[0].strip()
            termosBusca = k.split(",")[1].strip()
            termosBusca = remove_accents(termosBusca.decode('UTF-8'))
            arquivoSaida = oufile + str(i) + '-'
            print 'Tipo busca: '+tipoBusca
            print 'Termos busca: '+ termosBusca
            print 'Arquivo saida: '+arquivoSaida
            if buscaGrupos(tipoBusca, repercussoes, termosBusca, arquivoSaida):
                sucesso = True
            else:
                if tentativas == 5:
                    print "Numero de tentativas excedido. Execucao do Script abortada."
                    print "Verifique a conexao de internet e servidor CNPQ."
                    sys.exit(1)
                else:
                    tentativas = tentativas + 1
                    print "Erro no servidor do CNPQ... Tentando novamente em 30 segundos. Tentativa(s) " + str(tentativas) +" de 5"

