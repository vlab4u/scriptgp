#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Programa: ScriptGP2-busca.py
# Versao: 2014.09.22
#
# Função: O programa recebe uma lista de idgps do diretorio de grupos de pesquisa 
# e extrai informacoes descritivas do grupo
# Programador: André Santos 

import time
import sys
import re
import math
import unicodedata
import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

#=============================================================
#SETTINGS
#=============================================================

# #set the tab character to be used in output
# sep = '\t' 

# #caminho e prefixo para o arquivo de saída
# oufile = r'C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\dengue-15-03-2015-'

# #termo de busca
# busca = 'inovacao'

# #tipo de busca
# # 1 - todas as palavras
# # 2 - qualquer palavra
# # 3 - busca exata
tipoBusca = '2'
buscarepercussoes = True


#starts browser driver 
browser = webdriver.Firefox()

#open the searh main page
browser.get('http://dgp.cnpq.br/dgp/faces/consulta/consulta_parametrizada.jsf')

#locate the form object
login_form = browser.find_element_by_id('idFormConsultaParametrizada:idTextoFiltro')

#fill the form with search term
login_form.send_keys('catalysts fulereno fulerenos fullerene fullerenes grafeno graphene nanobiotecnologia nanocapsulas')

#select search option
select = Select(browser.find_element_by_xpath(".//*[@id='idFormConsultaParametrizada:basePesquisa']"))
select.select_by_value(tipoBusca)

#check if busca por repercussoes do grupo
if buscarepercussoes:
    browser.find_element_by_xpath((".//*[@id='idFormConsultaParametrizada:campos:3']")).click()

#submitt the form
browser.find_element_by_id("idFormConsultaParametrizada:idPesquisar").click()
browser.implicitly_wait(10)

#takes the main window reference
paginaprincipal = browser.current_window_handle

