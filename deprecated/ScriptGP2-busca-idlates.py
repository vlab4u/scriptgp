﻿# -*- coding: utf-8 -*-
# Programa: ScriptGP2.py
# Versao: 2014.09.04
#
# Funcao: O programa recebe uma lista de idllates e palavras chave de busca e verifica se os GPs de um idlattes possuem alguma destas palavras
# retorna os idlattes que posseum algum idgp associado.

#=============================================================
#SETTINGS
#=============================================================
#set the tab character to be used in output
sep = '\t' 
infile = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\corenano2.list"
infilesearch = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\lista-termosdebusca-nano.txt"
oufile = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano-.txt"    


#error codes used internally for the page load status
#0 - pages open correctly - successfully
#1 - server not responds or no internet connection
#2 - server responds but page or element page not found 


import time
import re
import unicodedata
import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC


def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])
def sanitizer(input_str):
    input_str = input_str.replace(u'\ufeff', '')
    return input_str
    
def procura (listadetermos, texto):
    achou = False
    global termo
    for termo in listadetermos:
        if  re.search(r'\b'+termo+r'\b',texto):
            achou = True
            break
    return achou
    
def open_espelho_rh(browser, idlattes):
    #browser is a reference of webdriver object
    #error codes returned by function
    #0 - pages open correctly - successfully
    #1 - server not responds or no internet connection
    #2 - server responds but page not found 
    #open the researchers main page
    saida = False
    tentativas = 0
    codigoerro = 0
    #this part tries to open browser and go to url
    while not saida:
        try:
            saida = True
            sucesso = True
            time.sleep(5)
            browser.get('http://dgp.cnpq.br/dgp/espelhorh/'+idlattes)
            print 'EspelhoRH idlattes esperado: ' + 'http://dgp.cnpq.br/dgp/espelhorh/'+idlattes
            print 'EspelhoRH idlattes brower  : ' + browser.current_url
        except:
            #there was a internal problem. will try to open a new browser object more 2 times
            print 'Erro de conexao em http://dgp.cnpq.br/dgp/espelhorh/'+idlattes
            print 'Servidor nao responde ou conexao de internet inoperante. Tentanto novamente...'
            saida = False
            tentativas = tentativas + 1
            if tentativas > 2:
                saida = True
                sucesso = False
                codigoerro = 1  
    if sucesso:
        #test if the page loads correctly - checks if the browser current url is the same that was passed to
        urlpassedto = 'http://dgp.cnpq.br/dgp/espelhorh/'+idlattes
        if urlpassedto != browser.current_url:
            sucesso = False
            codigoerro = 2
            carregou = False
            print 'Erro ao tentar acessar a pagina espelho RH de idlattes '+idlattes
            print 'Pagina inexistente ou erro no servidor'
        else:
            browser.implicitly_wait(10)
            carregou = True
            try:
                browser.find_element_by_id('menuEspelhoRH')
                
            except:
                carregou = False
                print 'deu merda'
            if not carregou:
                print 'Erro ao tentar acessar a pagina espelho RH de idlattes '+idlattes
                print 'Pagina inexistente ou erro no servidor'
                codigoerro=2
    return codigoerro

#=============================================================
#Modelo para scrapping
#=============================================================   
#the structure was initially got by command: campos = browser.find_elements_by_class_name("control-label") #find the labels
#is expected the follow structure: [lists in python starts with 0]
'''
        0 Situacao do grupo:
        1 Ano de formacao:
        2 Data da Situacao:
        3 Data do Ultimo envio:
        4 Lider(es) do grupo:
        5 Area predominante:
        6 Instituicao do grupo:
        7 Unidade:
        8 (empty)
        9 (empty)
        10 empty)
        11 Logradouro:
        12 Numero:
        13 Complemento:
        14 Bairro:
        15 UF:
        16 Localidade:
        17 CEP:
        18 Caixa Postal:
        19 Latitude:
        20 Longitude:
        21 Telefone:
        22 Fax:
        23 Contato do grupo:
        24 Website:
        25 (empty)
        26 (empty)
    '''
cabecalho =  (  "idgp" + sep + 
                "nomegrupo"+ sep +
                "situacao"+ sep +
                "ano"+ sep +
                "datasituacao"+ sep +
                "dataatualizacao"+ sep +
                "lider"+ sep +
                "area"+ sep +
                "instituicao"+ sep +
                "unidade"+ sep +
                "logradouro"+ sep +
                "numero"+ sep +
                "complemento"+ sep +
                "bairro"+ sep +
                "uf"+ sep +
                "localidade"+ sep +
                "cep"+ sep +
                "caixapostal"+ sep +
                "latitude"+ sep +
                "longitude"+ sep +
                "telefone"+ sep +
                "fax"+ sep +
                "contato"+ sep +
                "website")
#=============================================================
#MAIN PROGRAM
#=============================================================
with open(infile, 'r') as fi:
    listaidlattes = []
    for k in fi:
        listaidlattes.append(k[:16])
with open(infilesearch, 'r') as fi:
    listatermos = []
    for k in fi:
        listatermos.append(sanitizer(remove_accents(k.decode('utf-8')).strip().lower()))
linhas = cabecalho
listaidlattes_achados = []
listaidlattes_naoachados = []
listaresultados=[]
listaidlattes_erros = []
for idlattes in listaidlattes:
    browser = webdriver.Firefox()
    erro = open_espelho_rh(browser, idlattes)
    if erro == 0:    
        #takes the main window reference
        paginaprincipal = browser.current_window_handle
        #make a list of reseracher's groups (html elements, not the id's)
        listagrupos = browser.find_elements_by_xpath("/html/body/div[3]/div/div/div[1]/div/div[2]/div/form/span[2]/div/div/div/table/tbody/tr[*]/td[4]/button")
        #iterate over the list - opens a new window browser for each researcher's group
        print 'Pesquisador (idlattes)' + idlattes
        for botaogrupo in listagrupos:
            erro = 0
            achados = False
            searchabletext = ''
            #simulates the click on group mirror and open a new browser window
            botaogrupo.click()
            time.sleep(2)
            while True:
                if len(browser.window_handles) <= 1:
                    print 'Esperando resposta do servidor'
                    time.sleep(10)
                else:
                    break
            #<------|   
            browser.switch_to_window(browser.window_handles[1]) # set the focus on the new window (group window)
            #wait page loading
            browser.implicitly_wait(10)
            #tests if page loaded successfully
            teste = ''
            try:
                #if the correctly loads, need to have class elements named "controls". Otherwise, the page doesnt load correctly.
                teste = browser.find_elements_by_class_name("controls")[5].text 
            except:
                teste = 'falhou'
            if teste == 'falhou':
                print 'erro ao tentar acessar os grupos do '+idlattes
                erro = 2
            else:
                #extract the values of main simple fields, according to cabecalho
                area = browser.find_elements_by_class_name("controls")[5].text #find the content (values)
                nomegp = browser.find_elements_by_xpath('html/body/div[3]/div/div/div/div/div[2]/form/div/div[1]/h1')[0].text
                idgp = browser.current_url[-16:]
                print '    NomeGrupo: '+nomegp + ' Idgp :'+idgp
                print '    URL do grupo (browser): ' + browser.current_url 
                try:
                    resumo = browser.find_element_by_xpath('/html/body/div[3]/div/div/div/div/div[2]/form/div/div[4]/span[3]/div/fieldset/p').text
                except:
                    resumo = ''
                #the layout of some pages can vary on research line position 
                #in some cases, the research line is found at xpath html/body/div[3]/div/div/div/div/div[2]/form/div/div[4]/span[4]/div/fieldset/div/div/table/thead/tr/th[1]/span'
                #in other cases, the research line is found at xpath /html/body/div[3]/div/div/div/div/div[2]/form/div/div[4]/span[4]/div/fieldset/div/div/table/tbody/tr[*]/td[1]')
                #this part os script test for what parser fits the layout
                elemento4 = browser.find_element_by_xpath('html/body/div[3]/div/div/div/div/div[2]/form/div/div[4]/span[4]/div/fieldset/div/div/table/thead/tr/th[1]/span').text
                #test if four element is a research line, o
                if 'Nome da linha de pesquisa' in elemento4:
                    linhaspesquisa = browser.find_elements_by_xpath('/html/body/div[3]/div/div/div/div/div[2]/form/div/div[4]/span[4]/div/fieldset/div/div/table/tbody/tr[*]/td[1]')
                else:
                    linhaspesquisa = browser.find_elements_by_xpath('/html/body/div[3]/div/div/div/div/div[2]/form/div/div[4]/span[3]/div/fieldset/div/div/table/tbody/tr[*]/td[1]')
                try:
                    if 'Nome da linha de pesquisa' in elemento4:
                        listalinhaspesq = browser.find_elements_by_xpath("/html/body/div[3]/div/div/div/div/div[2]/form/div/div[4]/span[4]/div/fieldset/div/div/table/tbody/tr[*]/td[4]/a/button")
                    else:
                        listalinhaspesq = browser.find_elements_by_xpath("/html/body/div[3]/div/div/div/div/div[2]/form/div/div[4]/span[3]/div/fieldset/div/div/table/tbody/tr[*]/td[4]/a/button")
                    
                except:
                    listalinhaspesq = ''
                searchabletext = nomegp + ' ' + resumo + ' ' + area + ' ' + ' '.join(e.text for e in linhaspesquisa)
                searchabletext = remove_accents(searchabletext.lower())
                #Search in the group main informations. If found, stops the loop. If not, continue to explore each research line
                if procura(listatermos, searchabletext):
                    achados = True
                    onde = 'Pagina principal do grupo'
                    browser.close() #close the group windows (to minimize resources usage like RAM)
                    browser.switch_to_window(paginaprincipal)
                    break
        #<----------|
                elif len(listalinhaspesq) > 0:
                    paginadogrupo = browser.current_window_handle
                    for botaolinha in listalinhaspesq:
                        botaolinha.click()
                        time.sleep(2)
                        while True:
                            if len(browser.window_handles) <= 2:
                                print 'Esperando resposta do servidor'
                                time.sleep(10)
                            else:
                                break
                        
                        browser.switch_to_window(browser.window_handles[2]) # set the focus on the new window (group window)
                        #wait page loading
                        browser.implicitly_wait(10)
                        #tests if page loaded successfully
                        teste = ''
                        print '        Linha de pesquisa ' + browser.current_url
                        try:
                            #if the correctly loads, need to have class elements named "controls". Otherwise, the page doesnt load correctly.
                            teste = browser.find_elements_by_class_name("controls")
                        except:
                            teste = 'falhou'
                        if teste == 'falhou':
                            print 'erro ao tentar acessar os grupos do '+idlattes
                            erro = 2
                        else:
                            #extract the keywords
                            try:
                                palavras = browser.find_elements_by_xpath('/html/body/div[3]/div/div/div[1]/div/div[2]/form/div/div[3]/span[1]/div/fieldset/ul/li')
                            except:
                                palavras = []
                            #extract objectives
                            try:
                                objetivos = browser.find_element_by_xpath('/html/body/div[3]/div/div/div[1]/div/div[2]/form/div/div[3]/div[1]/fieldset/div/span[1]/div/div').text
                            except:
                                objetivos=''
                            #extract setor de aplicacao
                            try:
                                aplicacao = browser.find_element_by_xpath('/html/body/div[3]/div/div/div[1]/div/div[2]/form/div/div[3]/div[1]/fieldset/div/span[1]/div/div').text
                            except:
                                aplicacao = ''
                            
                        
                        searchabletext = objetivos + ' '.join(e.text for e in palavras) + ' ' + aplicacao
                        searchabletext = remove_accents(searchabletext.lower())
                        browser.close() #close the research line window (to minimize resources usage like RAM)
                        browser.switch_to_window(paginadogrupo)
                        #Search in the research line
                        if procura(listatermos, searchabletext):
                            achados = True
                            onde = 'Linha de pesquisa do grupo'
                            break
                    #<------|
            
            browser.close() #close the group windows (to minimize resources usage like RAM)
            browser.switch_to_window(paginaprincipal)
            if achados:
                break
        if achados:
            listaresultados.append(idlattes + sep + 'FOUND' + sep + idgp + sep + nomegp + sep + termo)
            listaidlattes_achados.append(idlattes)
            print idlattes + sep + 'FOUND' + sep + onde + sep + idgp + sep + nomegp + sep + termo
        else:
           listaresultados.append(idlattes + sep + 'NOT_FOUND' + sep + idgp + sep + nomegp + sep + termo)
           listaidlattes_naoachados.append(idlattes)
           print idlattes + sep + 'NOT_FOUND' + sep + idgp + sep + nomegp + sep + termo
    if erro != 0:
        listaidlattes_erros.append(idlattes + sep + str(erro))
    browser.close() #close the research (espelhorh) page
#browser.close()
listaidlattes_achados = set(listaidlattes_achados)
listaidlattes_naoachados = set(listaidlattes_naoachados)
with open(oufile+'idlattesfound.list', 'w') as fo:
    for linhas in listaidlattes_achados:
        fo.write("%s\n" % linhas.encode('utf-8'))
with open(oufile+'idlattesnotfound.list', 'w') as fo:
    for linhas in listaidlattes_naoachados:
        fo.write("%s\n" % linhas.encode('utf-8'))
with open(oufile+'allresults.list', 'w') as fo:
    for linhas in listaresultados:
        fo.write("%s\n" % linhas.encode('utf-8'))
with open(oufile+'idlatteserrors.list', 'w') as fo:
    for linhas in listaidlattes_erros:
        fo.write("%s\n" % linhas.encode('utf-8'))
