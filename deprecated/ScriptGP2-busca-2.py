#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Programa: ScriptGP2-busca.py
# Versao: 2014.09.22
#
# Função: O programa recebe uma lista de idgps do diretorio de grupos de pesquisa 
# e extrai informacoes descritivas do grupo
# Programador: André Santos 

import time
import sys
import re
import math
import unicodedata
import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

#=============================================================
#SETTINGS
#=============================================================

# #set the tab character to be used in output
# sep = '\t' 

# #caminho e prefixo para o arquivo de saída
# oufile = r'C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\dengue-15-03-2015-'

# #termo de busca
# busca = 'inovacao'

# #tipo de busca
# # 1 - todas as palavras
# # 2 - qualquer palavra
# # 3 - busca exata
# tipoBusca = '2'

def buscaGrupos(tipoBusca,buscarepercussoes,termosBusca,oufile):
    erro = False
    try:
        #starts browser driver 
        browser = webdriver.Firefox()

        #open the searh main page
        browser.get('http://dgp.cnpq.br/dgp/faces/consulta/consulta_parametrizada.jsf')

        #locate the form object
        login_form = browser.find_element_by_id('idFormConsultaParametrizada:idTextoFiltro')

        #fill the form with search term
        login_form.send_keys(termosBusca)

        #select search option
        select = Select(browser.find_element_by_xpath(".//*[@id='idFormConsultaParametrizada:basePesquisa']"))
        select.select_by_value(tipoBusca)
        
        #check if busca por repercussoes do grupo
        if buscarepercussoes:
            browser.find_element_by_xpath((".//*[@id='idFormConsultaParametrizada:campos:3']")).click()

        #submitt the form
        browser.find_element_by_id("idFormConsultaParametrizada:idPesquisar").click()
        browser.implicitly_wait(10)

        #takes the main window reference
        paginaprincipal = browser.current_window_handle

        #encontra o texto com o total de resultados na página
        textoTotalResultados = browser.find_element_by_xpath(("//span[@class='ui-paginator-current']")).text

        #extrai o total de resultados como um número
        totalResultados = int(re.search('[\d].*',textoTotalResultados).group())

        #calcula o total de páginas em função do total de resultados (considerando 15 resultados por página)
        totpaginas = int(math.ceil(totalResultados / 15.0))
        
        print "Total de registros: " + textoTotalResultados
        print "Total de paginas : " + str(totpaginas)
    except:
        erro = True
        return (False)
    listagrupos=[]
    listagruposerros=[]
    
    
    for pagina in range(0,totpaginas,1):
        print "Extraindo resultados da pagina :" + str(pagina+1)
        #get the list of groups objects
        time.sleep(5)
        try:
            erro = False
            grupos  = browser.find_elements_by_xpath("html/body/div[3]/div/div/form[2]/span/div[1]/div[1]/ul/li[*]/div/div[1]/div/a")
        except:
            erro = True
            print "[Erro] Nao foi possivel obter a lista de botoes de grupos"
            return (False)
        nroerros = 0
        if not erro:
            nomegrupos = []
            for grupo in grupos:
                time.sleep(3)
                clicou = True
                try:
                    nomegrupo = grupo.text
                except:
                    nomegrupo = '[Erro] nao foi possível identificar o nome do grupo'
                try:
                    grupo.click() #open the link of relative group in results page by simulating a click on it
                except:
                    erro = True
                    clicou = False
                    print "[Erro]  nao foi possivel clicar no botao do grupo "+nomegrupo
                browser.switch_to_window(paginaprincipal)
                nomegrupos.append(nomegrupo)
            numerojanelasabertas = len(browser.window_handles)
            print "Janelas ativas no momento: " + str(numerojanelasabertas)
            for k in range(numerojanelasabertas-1,0,-1):
                browser.switch_to_window(browser.window_handles[k])
                tentativas = 0
                try:
                    if browser.current_url == 'http://dgp.cnpq.br/dgp/faces/errorPage.jsf':
                        print '[Erro] no servidor do CNPQ ao tentar acessar o grupo ' + nomegrupos[k-1]
                        erro = True
                    else:
                        nomegp = browser.find_elements_by_xpath('html/body/div[3]/div/div/div/div/div[2]/form/div/div[1]/h1')[0].text
                        idgp = browser.current_url[-16:]
                        listagrupos.append(idgp + sep + nomegp + sep + browser.current_url) #append the group url to listagrupos
                        print '[' + idgp + '] '+ nomegp
                        erro = False
                except:
                    erro = True
                
                browser.close()
                if erro:
                    listagruposerros.append(nomegrupos[k-1])
              
        try:
            browser.switch_to_window(paginaprincipal)
            browser.find_elements_by_class_name("ui-paginator-next")[0].click()
            browser.implicitly_wait(10)
        except:
            erro = True
            print "[Erro] ao tentar clicar na proxima pagina"
            return (False)
    with open(oufile+'idgp.list', 'w') as fo:
        for linhas in listagrupos:
            fo.write("%s\n" % linhas.encode('utf-8'))
    with open(oufile+'idgp-erros.list', 'w') as fo:
        for linhas in listagruposerros:
            fo.write("%s\n" % linhas.encode('utf-8'))
    print '--------------------------------------------------------------------------------'
    print 'Resumo do processamento'
    print 'Busca por....................: '+termosBusca
    print 'Tipo de busca................: '+tipoBusca  + ' (1-todas as palavras, 2-qualquer palavra, 3-busca exata)'
    print 'Total de registros(da busca).: '+str(totalResultados)
    print 'Total de paginas.............: '+str(totpaginas)
    print 'Total de grupos extraidos....: '+str(len(listagrupos))
    print 'Total de grupos com erros....: '+str(len(listagruposerros))
    print '--------------------------------------------------------------------------------'
    return True
    
#=============================================================
#MAIN PROGRAM
#=============================================================
arquivoConfig = sys.argv[1]
#arquivoConfig = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano.config"
opcoes={}
with open(arquivoConfig, 'r') as fi:
    config = []
    for k in fi:
        opcoes[k.split("=")[0].strip()] = k.split("=")[1].strip()
arquivolistabusca = opcoes['arquivolistabusca']
oufile = opcoes['arquivosaida']
#o decoce string escape permite interpretar o \t corretamente como tabulacao
sep = opcoes['sep'].decode('string_escape')
repercussoes_valor = opcoes['repercussoes'].decode('utf-8')
if remove_accents(repercussoes_valor).lower() == 'sim':
    repercussoes = True
else:
    repercussoes = False

with open(arquivolistabusca, 'r') as fi:
    i = 0
    for k in fi:
        i = i + 1
        tentativas = 1
        sucesso = False
        while not sucesso and tentativas <= 5:
            tipoBusca = k.split(",")[0].strip()
            termosBusca = k.split(",")[1].strip()
            termosBusca = remove_accents(termosBusca.decode('UTF-8'))
            arquivoSaida = oufile + str(i) + '-'
            print 'Tipo busca : '+tipoBusca
            print 'Termos busca : '+ termosBusca
            print 'Arquivo saida : '+arquivoSaida
            if buscaGrupos(tipoBusca, repercussoes, termosBusca, arquivoSaida):
                sucesso = True
            else:
                if tentativas == 5:
                    print "Numero de tentativas excedido. Execucao do Script abortada."
                    print "Verifique a conexao de internet e servidor CNPQ."
                    sys.exit(1)
                else:
                    tentativas = tentativas + 1
                    print "Erro no servidor do CNPQ... Tentando novamente em 30 segundos. Tentativa(s) " + str(tentativas) +" de 5"

