    #!/usr/bin/env python
    # -*- coding: utf-8 -*-
# Programa: ScriptGP2-busca.py
# Versao 2015.04.02
# Função: O programa recebe uma lista de termos de busca e extrai a lista de idgps dos grupos resultantes
# Uso: ScriptGP2-busca <arquivo de configuracao>
# Programador: André Santos 

import time
import sys
import re
import math
import unicodedata
import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])
class logerros:
    listaerros = []
    def msg(self, txt):
        print txt
        self.listaerros.append(txt)

logerr = logerros()
#=============================================================
#SETTINGS
#=============================================================

# #set the tab character to be used in output
# sep = '\t' 

# #caminho e prefixo para o arquivo de saída
# oufile = r'C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\dengue-15-03-2015-'

# #termo de busca
# busca = 'inovacao'

# #tipo de busca
# # 1 - todas as palavras
# # 2 - qualquer palavra
# # 3 - busca exata
# tipoBusca = '2'

def buscaGrupos(nomeBusca,tipoBusca,buscarepercussoes,termosBusca,oufile,pularPaginas):
    class retorno:
        sucesso = True
        pularPaginas = []
    retorno = retorno()
    try:
        sucesso = True
        #starts browser driver 
        browser = webdriver.Firefox()

        #open the searh main page
        browser.get('http://dgp.cnpq.br/dgp/faces/consulta/consulta_parametrizada.jsf')

        #locate the form object
        login_form = browser.find_element_by_id('idFormConsultaParametrizada:idTextoFiltro')

        #fill the form with search term
        login_form.send_keys(termosBusca)

        #select search option
        select = Select(browser.find_element_by_xpath(".//*[@id='idFormConsultaParametrizada:basePesquisa']"))
        select.select_by_value(tipoBusca)
        
        #check if busca por repercussoes do grupo
        if buscarepercussoes:
            browser.find_element_by_xpath((".//*[@id='idFormConsultaParametrizada:campos:3']")).click()

        #submitt the form
        browser.find_element_by_id("idFormConsultaParametrizada:idPesquisar").click()
        browser.implicitly_wait(10)

        #takes the main window reference
        paginaprincipal = browser.current_window_handle

        #encontra o texto com o total de resultados na página
        textoTotalResultados = browser.find_element_by_xpath(("//span[@class='ui-paginator-current']")).text

        #extrai o total de resultados como um número
        totalResultados = int(re.search('[\d].*',textoTotalResultados).group())

        #calcula o total de páginas em função do total de resultados (considerando 15 resultados por página)
        totpaginas = int(math.ceil(totalResultados / 15.0))
        
        logerr.msg("Total de registros: " + textoTotalResultados)
        logerr.msg("Total de paginas : " + str(totpaginas))
    except:
        sucesso = False
    if sucesso:
        listagrupos=[]
        listagruposerros=[]
        #abre o arquivo de log de paginas para ver se houve erro e recomeca na pagina seguinte
        for pagina in range(1,totpaginas+1,1):
            if pagina not in pularPaginas:
                logerr.msg( "Extraindo resultados da pagina :" + str(pagina))
                #get the list of groups objects
                time.sleep(5)
                try:
                    erro = False
                    grupos  = browser.find_elements_by_xpath("html/body/div[3]/div/div/form[2]/span/div[1]/div[1]/ul/li[*]/div/div[1]/div/a")
                except:
                    erro = True
                    
                nroerros = 0
                if not erro:
                    #guarda o nome do primeiro grupo para comparar com o primeiro grupo da pagina seguinte (se forem iguais houve erro)
                    try:
                        resultadosPaginaAnterior = grupos[0].text
                    except:
                        print 'essa porra nao carregou' 
                    
                    #percorre a lista de grupos da pagina de resultados e abre as paginas dos grupos em novas janelas
                    nomegrupos = []
                    for grupo in grupos:
                        time.sleep(3)
                        clicou = True
                        #recupera o nome do grupo antes de clicar no botao para abrir a pagina do grupo (nao possui idgp)
                        try:
                            nomegrupo = grupo.text
                        except:
                            nomegrupo = '[Erro] nao foi possível identificar o nome do grupo'
                            logerr.msg('[Erro] nao foi possivel identificar o nome do grupo')
                            
                        #open the link of relative group in results page by simulating a click on it
                        try:
                            grupo.click() 
                        except:
                            erro = True
                            clicou = False
                            logerr.msg("[Erro] nao foi possivel clicar no botao do grupo "+nomegrupo)
                        #retorna para a pagina de resultados
                        browser.switch_to_window(paginaprincipal)
                        nomegrupos.append(nomegrupo)
                    
                    numerojanelasabertas = len(browser.window_handles)
                    logerr.msg( "Janelas ativas no momento: " + str(numerojanelasabertas))
                    
                    #percorre as janelas abertas dos grupos
                    for k in range(numerojanelasabertas-1,0,-1):
                        browser.switch_to_window(browser.window_handles[k])
                        tentativas = 0
                        
                        #verifica se a janela aberta esta correta - deve ter a url de espelho do grupo
                        try:
                            #se estiver correta, pega o idgp, nome e url
                            if 'http://dgp.cnpq.br/dgp/espelhogrupo/' in browser.current_url:
                                nomegp = browser.find_elements_by_xpath('html/body/div[3]/div/div/div/div/div[2]/form/div/div[1]/h1')[0].text
                                idgp = browser.current_url[-16:]
                                listagrupos.append(idgp + sep + nomegp + sep + browser.current_url) #append the group url to listagrupos
                                logerr.msg( '[' + idgp + '] '+ nomegp)
                                erro = False
                            else:
                                logerr.msg('[Erro] no servidor do CNPQ ao tentar acessar o grupo ' + nomegrupos[k-1])
                                erro = True
                        except:
                            erro = True
                        
                        #fecha a janela do grupo
                        browser.close()
                        #se houve erro, registra na lista de erros o nome do grupo (regisra apenas o nome porque nao foi possivel extrair o idgp
                        if erro:
                            listagruposerros.append(nomegrupos[k-1])
            else:
                logerr.msg('[Erro] Pulando a pagina '+str(pagina))
            
            #avanca para a proxima pagina de resultados de busca
            try:
                browser.switch_to_window(paginaprincipal)
                browser.find_elements_by_class_name("ui-paginator-next")[0].click()
                browser.implicitly_wait(10)
            except:
                erro = True
                print "[Erro] ao tentar clicar na proxima pagina"
                pularPaginas.append(pagina)
                retorno.sucesso = False
                retorno.pularPaginas = pularPaginas
                return (retorno)
            #em algumas situacoes o clique funciona mas a pagina nao carrega. 
            #testa se a pagina seguinte carregou corretamente, verificando se realmente foram apresentados novos grupos
            try:
                erro = False
                time.sleep(5)
                gruposteste  = browser.find_elements_by_xpath("html/body/div[3]/div/div/form[2]/span/div[1]/div[1]/ul/li[*]/div/div[1]/div/a")
            except:
                erro = True
                logerr.msg("[Erro] Nao foi possivel obter a lista de botoes de grupos")
                pularPaginas.append(pagina)
                retorno.sucesso = False
                retorno.pularPaginas = pularPaginas
                return (retorno)
              
            #se a lista de resultados de grupos e igual a pagina anterior entao ocorreu um erro.
            try:
                erropaginaatual = False
                resultadosPaginaAtual = gruposteste[0].text
            except:
                resultadosPaginaAtual = ''
                erropaginaatual = True
            print erro
            print 'atual '+resultadosPaginaAtual
            print 'anterior '+resultadosPaginaAnterior
            if  erropaginaatual:
                logerr.msg( "[Erro] ao carregar a proxima pagina de busca. Impossivel continuar.")
                pularPaginas.append(pagina)
                retorno.sucesso = False
                retorno.pularPaginas = pularPaginas
                return (retorno)
                
                
            nroerros = 0
            
        with open(oufile+'idgp.list', 'w') as fo:
            for linhas in listagrupos:
                fo.write("%s\n" % linhas.encode('utf-8'))
        with open(oufile+'idgp-erros.list', 'w') as fo:
            for linhas in listagruposerros:
                fo.write("%s\n" % linhas.encode('utf-8'))
        logerr.msg( '--------------------------------------------------------------------------------')
        logerr.msg('RESUMO do processamento')
        logerr.msg('Nome da busca................: '+nomeBusca)
        logerr.msg('Busca por....................: '+termosBusca)
        logerr.msg('Tipo de busca................: '+tipoBusca  + ' (1-todas as palavras, 2-qualquer palavra, 3-busca exata)')
        logerr.msg('Total de registros(da busca).: '+str(totalResultados))
        logerr.msg('Total de paginas.............: '+str(totpaginas))
        logerr.msg('Erros nas Paginas nro........: '+str(pularPaginas))
        logerr.msg('Total de grupos extraidos....: '+str(len(listagrupos)))
        logerr.msg('Total de grupos com erros....: '+str(len(listagruposerros)))
        logerr.msg('--------------------------------------------------------------------------------')
        retorno.sucesso = True
        retorno.pularPaginas = pularPaginas
        return retorno
    else:
        retorno.sucesso = False
        retorno.pularPaginas = pularPaginas
        return retorno
        
    
#=============================================================
#MAIN PROGRAM
#=============================================================
arquivoConfig = sys.argv[1]
#arquivoConfig = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano.config"
opcoes={}
with open(arquivoConfig, 'r') as fi:
    config = []
    for k in fi:
        opcoes[k.split("=")[0].strip()] = k.split("=")[1].strip()
arquivolistabusca = opcoes['arquivolistabusca']
oufile = opcoes['arquivosaida']
#o decoce string escape permite interpretar o \t corretamente como tabulacao
sep = opcoes['sep'].decode('string_escape')
repercussoes_valor = opcoes['repercussoes'].decode('utf-8')
if remove_accents(repercussoes_valor).lower() == 'sim':
    repercussoes = True
else:
    repercussoes = False

with open(arquivolistabusca, 'r') as fi:
    i = 0
    for k in fi:
        i = i + 1
        tentativas = 1
        sucesso = False
        pularPaginas = []
        while not sucesso and tentativas <= 5:
            nomeBusca = k.split(",")[0].strip()
            tipoBusca = k.split(",")[1].strip()
            termosBusca = k.split(",")[2].strip()
            termosBusca = remove_accents(termosBusca.decode('UTF-8'))
            arquivoSaida = oufile + str(i) + '-'
            logerr.msg( '--------------------------------------------------------------------------------')
            logerr.msg('INICIO do processamento')
            logerr.msg('Nome da busca..: '+nomeBusca)
            logerr.msg('Tipo busca.....: '+tipoBusca)
            logerr.msg('Termos busca...: '+ termosBusca)
            logerr.msg('Arquivo saida..: '+arquivoSaida)
            logerr.msg( '--------------------------------------------------------------------------------')
            busca = buscaGrupos(nomeBusca, tipoBusca, repercussoes, termosBusca, arquivoSaida,pularPaginas)
            if busca.sucesso:
                sucesso = True
            else:
                if tentativas == 5:
                    logerr.msg("Numero de tentativas excedido. Execucao do Script abortada.")
                    logerr.msg("Verifique a conexao de internet e servidor CNPQ.")
                    sys.exit(1)
                else:
                    tentativas = tentativas + 1
                    logerr.msg("Erro no servidor do CNPQ... Tentando novamente em 10 minutos. Tentativa(s) " + str(tentativas) +" de 5")
                    time.sleep(600)
                    pularPaginas = busca.pularPaginas
with open(oufile+'log.txt', 'w') as fo:
    for linhas in logerr.listaerros:
        fo.write("%s\n" % linhas.encode('utf-8'))
