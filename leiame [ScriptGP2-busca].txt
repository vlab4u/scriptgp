Programa: ScriptGP2-busca   
Versão: 2015.02.18

DESCRIÇÃO 

O programa faz uma busca na base do Diretório dos Grupos de Pesquisa do CNPQ (DGP) e retorna todos as identificações (idgp) dos grupos que atendam os requisitos da busca.

É utilizado o próprio sistema de busca da base DGP (http://dgp.cnpq.br/dgp/faces/consulta/consulta_parametrizada.jsf)


CRITÉRIOS DE BUSCA

A busca é feita no nome do grupo, nome da linha de pesquisa, palavra-chave da linha de pesquisa e repercusões do grupo.

São considerados apenas os grupos certificados. Os grupos desatualizados também são incluídos na busca.


APLICAÇÃO

Este programa foi desenvolvido para auxiliar na identificacao de grupos de pesquisa através da busca por palavras-chave.


INSTALAÇÃO

O ScriptGP2-busca.py precisa dos seguintes programas
1. Python 2.7 (https://www.python.org/)
2. Navegador Firefox (https://www.mozilla.org/)
    As versões mais recentes (36 e 37(beta) ) do Firefox podem apresentar alguns problemas de compatibilidade com a biblioteca Selenium para Python.
    O problema ocorre quando o ScriptGP2-busca tenta abrir uma janela do navegador. A janela fica em branco e não processa o Script.
    Neste caso recomenda-se o uso da versão 35, que apresenta menos problemas de compatiblidade.
    Você pode obter este versão no endereço: https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/35.0/
3. Biblioteca Selenium para Python (https://pypi.python.org/pypi/selenium)


CONFIGURAÇÃO

O programa precisa de um arquivo como de configuracao e outro com os termos de busca
1 - arquivo de configuracao
    O arquivo deverá conter as seguintes linhas
    sep = \t
    arquivolistabusca = C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\lista-busca-nano.txt
    arquivosaida = C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano20150319-    apenas um idlattes por linha.
    
    sep é o caracter que será utilizado para separar os campos no arquivo de saída. 
        recomenda-se utilizar tabulacao \t 
    arquivolistabusca é o caminho para o arquivo com os termos de busca
    arquivolistabusca é o caminho para o arquivo de saida, incluindo o prefixo desejado para o nome do arquivo.
2 - arquivo com os termos de busca
    Este é o arquivo informa as buscas e os termos de busca a serem realizados.
    Cada linha deve conter o código para o tipo da busca e os termos daquela busca
    
    O código para o tipo de pesquisa indica:
    1 - retorna os grupos que contenham TODAS as palavras de busca.
    2 - retorna os grupos que contenham QUALQUER uma das palavras de busca.
    3 - retorna os grupos que contenham a FRASE exata de busca.
    
    Após o código de busca, deverá ser inserida uma vírgula e em seguida devem ser colocadas as palavras de busca, separadas apenas por espaços.
    Exemplo
    1, palavra1 palavra2 palavra3
    
    Observações
    O sistema do CNPQ não aceita aspas ou caracteres especiais como *, ?.
    Caso deseje pesquisar uma palavra composta ou frase, use o código 3.
    Exemplo
    3, nanotubos de carbono
    
    O sistema do CNPQ aceita apenas 100 caracteres por vez na expressao de busca.
    Assim, se sua expressao de busca contiver mais do que 100 caracteres você deverá dividi-la em mais linhas.
    

USO

ScriptGP2-busca.py arquivoconfig.txt
