﻿#=============================================================
#SCRIPTGP2-idlates-idgp
#=============================================================
# Programa: ScriptGP2-idlates-idgp.py
# Versao: 2015.02.18
#
# Funcao: O programa retorna uma lista (idgp) com todos os grupos de pesquisa aos quais um pesquiador (idlattes) pertence.
# Para mais informacoes veja leiame[ScriptGP2-idlattes-idgp].txt



#=============================================================
#SETTINGS
#=============================================================
#set the tab character to be used in output (you may choose diferents field separators for your convenience 
sep = '\t' 

#path of the file with an idlattes list (the idllate may be he first 16 characters in each line)
infile = r"C:\Users\amsantos\Google Drive\Programas\ScriptGP20\Teste Nano\nanogpbp-teste.list"

#path and prefix of the files where the results will be puted 
#if you put 'c:\mydata\jan-205' all output files will be put in directory c:\mydata and will have the 'jan-2015' prefix. 
oufile = r'C:\Users\amsantos\Google Drive\Programas\ScriptGP20\Teste Nano\teste-idgp-dos-idlattes-17-02-2015'


#error codes used internally for the page load status
#0 - pages open correctly - successfully
#1 - server not responds or no internet connection
#2 - server responds but page or element page not found 

# Estrutura de campos da página do grupo
# the structure was initially got by command: campos = browser.find_elements_by_class_name("control-label") #find the labels
# is expected the follow structure:
'''
        0 Situacao do grupo:
        1 Ano de formação:
        2 Data da Situacao:
        3 Data do último envio:
        4 Lider(es) do grupo:
        5 Area predominante:
        6 Instituicao do grupo:
        7 Unidade:
        8 (empty)
        9 (empty)
        10 empty)
        11 Logradouro:
        12 Numero:
        13 Complemento:
        14 Bairro:
        15 UF:
        16 Localidade:
        17 CEP:
        18 Caixa Postal:
        19 Latitude:
        20 Longitude:
        21 Telefone:
        22 Fax:
        23 Contato do grupo:
        24 Website:
        25 (empty)
        26 (empty)
    '''

import time
import re
import unicodedata
import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC


def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])
def sanitizer(input_str):
    #Removes U+FEFF character
    #The Unicode character U+FEFF is the byte order mark, or BOM, 
    #and is used to tell the difference between big- and little-endian UTF-16 encoding.
    #Can cause some problems when scrapping text from web page.
    input_str = input_str.replace(u'\ufeff', '')
    return input_str
def procura (listadetermos, texto):
    achou = False
    global termo
    for termo in listadetermos:
        if  re.search(r'\b'+termo+r'\b',texto):
            achou = True
            break
    return achou
def open_espelho_rh(browser, idlattes):
    #browser is a reference of webdriver object
    #error codes returned by function
    #0 - pages open correctly - successfully
    #1 - server not responds or no internet connection
    #2 - server responds but page not found 
    #open the researcher´s main page
    saida = False
    tentativas = 0
    codigoerro = 0
    #this part tries to open browser and go to url
    while not saida:
        try:
            saida = True
            sucesso = True
            time.sleep(5)
            browser.get('http://dgp.cnpq.br/dgp/espelhorh/'+idlattes)
            print 'EspelhoRH idlattes esperado: ' + 'http://dgp.cnpq.br/dgp/espelhorh/'+idlattes
            print 'EspelhoRH idlattes brower  : ' + browser.current_url
        except:
            #there was a internal problem. will try to open a new browser object more 2 times
            print 'Erro de conexao em http://dgp.cnpq.br/dgp/espelhorh/'+idlattes
            print 'Servidor nao responde ou conexão de internet inoperante. Tentanto novamente...'
            saida = False
            tentativas = tentativas + 1
            if tentativas > 2:
                saida = True
                sucesso = False
                codigoerro = 1      
    
    if sucesso:
        #test if the page loads correctly - checks if the browser current url is the same that was passed to
        urlpassedto = 'http://dgp.cnpq.br/dgp/espelhorh/'+idlattes
        if urlpassedto != browser.current_url:
            sucesso = False
            codigoerro = 2
            carregou = False
            print 'Erro ao tentar acessar a pagina espelho RH de idlattes '+idlattes
            print 'Pagina inexistente ou erro no servidor'
        else:
            browser.implicitly_wait(10)
            carregou = True
            try:
                browser.find_element_by_id('menuEspelhoRH')
                
            except:
                carregou = False
                print 'Problemas'
            if not carregou:
                print 'Erro ao tentar acessar a pagina espelho RH de idlattes '+idlattes
                print 'Pagina inexistente ou erro no servidor'
                codigoerro=2
    return codigoerro


#=============================================================
#MAIN PROGRAM
#=============================================================
with open(infile, 'r') as fi:
    listaidlattes = []
    for k in fi:
        listaidlattes.append(k[:16])
listaidlattes_achados = []
listaidlattes_naoachados = []
listaresultados=[]
listaidlattes_erros = []
listaScriptlattes = []
for idlattes in listaidlattes:
    browser = webdriver.Firefox()
    erro = open_espelho_rh(browser, idlattes)
    if erro == 0:    
        
        #takes the main window reference
        paginaprincipal = browser.current_window_handle
        
        #make a list of reseracher´s groups (html elements, not the id´s)
        listagrupos = browser.find_elements_by_xpath("/html/body/div[3]/div/div/div[1]/div/div[2]/div/form/span[2]/div/div/div/table/tbody/tr[*]/td[4]/button")
        
        #armazena o nome do pesquisador para verificar se eh lider do grupo
        nomePesquisador = browser.find_element_by_xpath('/html/body/div[3]/div/div/div[1]/div/div[2]/div/div[1]/h1').text
        nomePesquisador = sanitizer(remove_accents(nomePesquisador))
        
        print 'Pesquisador-(idlattes)' + nomePesquisador + "-(" + idlattes + ")"
        
        listaIdgp = []
        for botaogrupo in listagrupos:
            erro = 0
            achados = False
            searchabletext = ''
            linha = ''
            
            #simulates the click on group mirror and open a new browser window
            botaogrupo.click()
            time.sleep(2)
            
            while True:
                if len(browser.window_handles) <= 1:
                    print 'Esperando resposta do servidor'
                    time.sleep(10)
                else:
                    break
            #<------|   
            
            browser.switch_to_window(browser.window_handles[1]) # set the focus on the new window (group window)
            #wait page loading
            browser.implicitly_wait(10)
            
            #tests if page loaded successfully
            teste = ''
            falhou = False
            try:
                #if the correctly loads, need to have class elements named "controls". Otherwise, the page doesnt load correctly.
                teste = browser.find_elements_by_class_name("controls")[5].text 
            except:
                falhou = True
            
            if falhou:
                print 'erro ao tentar acessar os grupos do '+idlattes
                erro = 2
            else:
                #extract the values of main simple fields, according to cabecalho
                campos = browser.find_elements_by_class_name("controls") 
                lider = sanitizer(remove_accents(campos[4]))
                area = campos[5].text #find the content (values)
                instituicao = campos[6].text.split(" - ")[1]
                nomegp = browser.find_elements_by_xpath('html/body/div[3]/div/div/div/div/div[2]/form/div/div[1]/h1')[0].text
                idgp = browser.current_url[-16:]
                print '    NomeGrupo: '+nomegp + ' Idgp :'+idgp
                print '    URL do grupo (browser): ' + browser.current_url
                listaresultados.append(idlattes + sep + idgp + sep + nomegp )
                listaIdgp.append(remove_accents((instituicao + " - " + nomegp + " - pes-grp[" + idgp + "]")))
            browser.close() #close the group windows (to minimize resources usage like RAM)
            browser.switch_to_window(paginaprincipal)
    if erro != 0:
        listaidlattes_erros.append(idlattes + sep + str(erro))
    else:
        primeiro = True
        for k in listaIdgp:
            if primeiro:
                linha = idlattes + ', , , ' + k
                primeiro = False
            else:
                linha = linha + " :: " + k
        listaScriptlattes.append(linha)
    browser.close() #close the research (espelhorh) page
#browser.close()
with open(oufile+'idgpresults.list', 'w') as fo:
    for linhas in listaresultados:
        fo.write("%s\n" % linhas.encode('utf-8'))
with open(oufile+'idlatteserrors.list', 'w') as fo:
    for linhas in listaidlattes_erros:
        fo.write("%s\n" % linhas.encode('utf-8'))
with open(oufile+'idgpScripLattes.list', 'w') as fo:
    for linhas in listaScriptlattes:
        fo.write("%s\n" % linhas.encode('utf-8'))
