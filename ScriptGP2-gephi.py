﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Programa: ScriptGP2-pvt-pesquisadores.py

#
# Função: O programa recebe uma lista de cadastro de grupos e outra com pesquisadores monta uma pivottable.
# A lista de cadastro deve conter o idgp e demais dados como instituicao, estado, ano, etc
# A lista de pesquisadores deve conter o idlattes e o grupo ao qual pertence (idgp)


#=============================================================
#SETTINGS
#=============================================================

#set the tab character to be used in input
sep = '\t' 

#arquivo de entrada com a lista de grupos de pesquisa no formato 
infile_grupos = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\Uninove\UNINOVE-ADM-GRUPOS-idgpcadastro.list"

#arquivo com a lista de pesquisadores e os grupos aos quais pertencem 
infile_pesquisadores = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\Uninove\UNINOVE-ADM-idlattes.list"

#arquivo com a lista de pesquisadores e os grupos aos quais pertencem 
infile_instituicoes = ''
infile_estudantes = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\Uninove\UNINOVE-ADM-ESTUDANTES-idgp-idlattes-est.list"

#diretorio para os arquivos de saída. Deve terminar com '\'

oudir = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\Uninove\\"
ouprefix = r"U9-PPGA-"
htmldir = './'

oufile = oudir+ouprefix+".gexf"

import re
import unicodedata
import networkx as nx

def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str,'UTF'))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])
    

#=============================================================
#MAIN PROGRAM
#=============================================================
grupos=dict()
grupoPesquisadore = dict()
pivottableJson=[]
listaIdlattesErros=[]
g=nx.Graph()

#cria nodes e edges a partir do cadastro dos grupos
#nodes importados : idgp, uf, e siglaInstituicaoGrupo 
with open(infile_grupos, 'r') as fi:
    for k in fi:
        c = k.split(sep)
        idgp = c[0]
        nomeGrupo = c[1]
        universidade_nome = remove_accents(c[9]) #siglaInstituicaoGrupo
        universidade_sigla = c[10]
        uf = c[12]
        grupos[idgp]=c
        
        #adiciona nodes
        #grupo
        g.add_node(idgp,label = nomeGrupo, tipo = 'gp')
        # uf 
        g.add_node(uf, label = uf, tipo = 'uf')
        #adiciona a universidade (sigla)
        g.add_node(universidade_nome, label = universidade_sigla, tipo = 'un')
        
        #adiciona os edges
        #grupo-universidade
        g.add_edge(idgp,universidade_nome,label='grupo-univ', tipo='grupo-univ')
        #grupo-uf
        g.add_edge(idgp,uf,label='grupo-uf', tipo='grupo-uf')
        #universidade-uf
        g.add_edge(universidade_nome, uf, label='univ-uf', tipo='univ-uf')
with open(infile_pesquisadores, 'r') as fi:
    for k in fi:
        c = k.split(sep)
        idgp = c[0]
        idlattes = c[1]
        nomePesquisador = remove_accents(c[2])
       
        
        #adiciona nodes
        #pesquisador
        g.add_node(idlattes,label = nomePesquisador, tipo = 'pe')
        
        #adiciona os edges
        #grupo-pesquisador
        g.add_edge(idgp,idlattes,label='grupo-pesq', tipo='grupo-pesq')
        #pesquisador-universidade_do_grupo
        g.add_edge(idlattes, grupos[idgp][9],label='pesq-univ_gp', tipo = 'pesq-univ_gp')
with open(infile_estudantes, 'r') as fi:
    for k in fi:
        c = k.split(sep)
        idgp = c[0]
        idlattes = c[1]
        nomeEstudante = remove_accents(c[2])
        
        #adiciona nodes
        #estudante
        g.add_node(idlattes,label = nomeEstudante, tipo = 'es')
        
        #adiciona os edges
        #grupo-estudante
        g.add_edge(idgp,idlattes,label='grupo-est', tipo='grupo-est')
        #estudante-universidade_do_grupo
        g.add_edge(idlattes, grupos[idgp][9],label='est-univ_gp', tipo = 'est-univ_gp')        
        
nx.write_gexf(g,oufile)