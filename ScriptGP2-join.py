#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Programa: ScriptDepup.py
# Versao: 
#
# Função: O programa recebe várias lista de idgps do diretorio de grupos de pesquisa e junta todos em uma unica lista, eliminando os duplicados. 
# Programador: André Santos 
# O programa gera duas listas de saída. Uma com os idgps únicos e outra com os duplicados encontrados. 
# O programa deve receber um arquivo onde cada linha e o endereco de um dos arquivos a serem juntados.

#=============================================================
#SETTINGS
#=============================================================

# caminho e prefixo para o arquivo de saída
oufile = r'C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano-final-'
arquivolistas = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\arquivos_nano_para_juntar.txt"

import unicodedata
def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])
#=============================================================
#MAIN PROGRAM
#=============================================================
#arquivoConfig = sys.argv[1]
listatotal = []
with open(arquivolistas, 'r') as fi1:
    for k in fi1:
        with open(k.strip(), 'r') as fi2:
            for j in fi2:
                listatotal.append(remove_accents(j.strip().decode('UTF-8')))
                print j
listaunica = set(listatotal)
print 'Total de registros lidos...: ' + str(len(listatotal))
print 'Total de registros unicos..: ' + str(len(listaunica))
with open(oufile+'idgps-unicos.list', 'w') as fo:
    for linhas in listaunica:
        fo.write("%s\n" % linhas.encode('utf-8'))
