﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Programa: ScriptGP2-cleaner.py
#
# Função: O programa recebe qualquer lista de idgp e elimina registros duplicados ou que nao correspondam ao padrao de 16 numeros no idgp
# o programa tambem elimina espacos em branco ou caracteres especiais no inicio ou fim de cada linha da lista.

#=============================================================
#SETTINGS
#=============================================================

#set the tab character to be used in output
sep = '\t' 

#arquivo de entrada com a lista de idgp
infile = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano-dedup-idgps-unicos-idgpcadastro.list"

#caminho e prefixo para os arquivos de saída
oufile = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano-dedup-idgps-unicos-idgpcadastro-"

#=============================================================
#MAIN PROGRAM
#=============================================================
with open(infile, 'r') as fi:
    listalimpa = []
    for k in fi:
        if (k[:16].isdigit()):
            listalimpa.append(k.strip().decode('utf8'))
        print k

#grava os arquivos de saida
with open(oufile+'cleaned.list', 'w') as fo:
    for linhas in listalimpa:
        fo.write("%s\n" % linhas.encode('utf-8'))

